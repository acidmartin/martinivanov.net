<?php

    /*
     * Create an array of CSS files and output their content
     **/
    header('Content-type: text/css');
    ob_start('ob_gzhandler');
    $files = explode(',', $_GET['files']);
    
    foreach($files as $file) {
        require($file);
    }
?>