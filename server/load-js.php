<?php

    /*
     * Create an array of JS files and output their content
     **/
    header('Content-type: text/javascript');
    ob_start('ob_gzhandler');
    $files = explode(',', $_GET['files']);
    
    foreach($files as $file) {
        require($file);
    };
?>